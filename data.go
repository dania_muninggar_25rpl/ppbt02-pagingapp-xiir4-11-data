package main
import "encoding/json"
import "net/http"
import "fmt"

type suku struct {
ID string
Name string
Asal string
}
var data = []suku{
	suku{"B001", "Suku Abai", "Kalimantan Timur"},
	suku{"B002", "Suku Abung", "Sumatera"},
	suku{"B003", "Suku Aceh", "Aceh"},
	suku{"B004", "Suku Adang", "Kalimantan"},
	suku{"B005", "Suku Adonara", "Nusa Tenggara"},
	suku{"B006", "Suku Ahe", "Kabupaten Berau"},
	suku{"B007", "Suku Akit", "Sumatera Utara"},
	suku{"B008", "Suku Sambori", "Bima"},
	suku{"B009", "Suku Wowoni", "Sulawesi Tenggara"},
	suku{"B010", "Suku Sunda", "Jawa Barat"},
	suku{"B011", "Suku Jawa", "Jawa"},
	suku{"B012", "Suku Madura", "Jawa Timur"},
	suku{"B013", "Suku Tengger", "Jawa Timur"},
	suku{"B014", "Suku Dayak", "Kalimantan Barat"},
	suku{"B015", "Suku Batak", "SUmatera Barat"},
	suku{"B016", "Suku Betawi", "DKI Jakarta"},
	suku{"B017", "Suku Sungkai ", "Lampung"},
	suku{"B018", "Suku Sumbawa", "NTB"},
	suku{"B019", "Suku Sula", "Maluku"},
	suku{"B020", "Suku Smoung", "Lampung"},
	suku{"B021", "Suku Sigulai", "Aceh"},
	suku{"B022", "Suku Nias", "Sumatera Utara"},
	suku{"B023", "Suku Murung", "Kalimantan"},
	suku{"B024", "Suku Kerinci", "Jambi"},
	suku{"B025", "Suku Osing", "Jawa Timur"},
	suku{"B026", "Suku Orang Kanekes", "Jawa Barat"},
	suku{"B027", "Suku Suku Cirebon", "Jawa Barat"},
	suku{"B028", "Suku Etnis Tionghoa", "Jawa Tengah"},
	suku{"B029", "Suku Bagelen", "Jawa Tengah"},
	suku{"B030", "Suku Banyumas", "Jawa Tengah"},
	suku{"B031", "Suku Nagarigung", "Jawa Timur"},
	suku{"B032", "Suku Wong Samin", "Jawa Timur"},
	suku{"B033", "Suku Bawean", "Jawa Timur"},
	suku{"B034", "Suku Pontianak", "Kalimantan Barat"},
	suku{"B035", "Suku Pos", "Kalimantan Barat"},
	suku{"B036", "Suku Punti", "Kalimantan Barat"},
	suku{"B037", "Suku Randuk", "Kalimantan Barat"},
	suku{"B038", "Suku Ribun", "Kalimantan Barat"},
	suku{"B039", "Suku Sambas", "Kalimantan Barat"},
	suku{"B040", "Suku Cempedek", "Kalimantan Barat"},
	suku{"B041", "Suku Dalam", "Kalimantan Barat"},
	suku{"B042", "Suku Darat", "Kalimantan Barat"},
	suku{"B043", "Suku Darok", "Kalimantan Barat"},
	suku{"B044", "Suku Desa", "Kalimantan Barat"},
	suku{"B045", "Suku Kopak", "Kalimantan Barat"},
	suku{"B046", "Suku Jalan", "Kalimantan Timur"},
	suku{"B047", "Suku Touk", "Kalimantan Timur"},
	suku{"B048", "Suku Baka", "Kalimantan Timur"},
	suku{"B049", "Suku Kayan", "Kalimantan Timur"},
	suku{"B050", "Suku Tukung", "Kalimantan Barat"},
}
	func users(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		if r.Method == "POST" {
		var result, err = json.Marshal(data)
		if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
		}
		w.Write(result)
		return
		}
		http.Error(w, "", http.StatusBadRequest)
		}
		func main() {
			http.HandleFunc("/users", users)

			fmt.Println("starting web server at http://localhost:8080/")
			http.ListenAndServe(":8080", nil)
		}

